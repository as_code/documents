# Software Design {#software-design}


**ACE**

|            |            |
| ---        | ---        |
| _Date:_    | 2018-05-30 |
| _Version:_ | 8.5.17     |
| _Status:_  | Released   |
| _Author:_  | I/O Team   |
